app.config(function($locationProvider, $routeProvider){
  $locationProvider.html5Mode(true)
  $routeProvider
    .when('/', {
      templateUrl: '/home.html',
      controller: 'Home'
    })
    .when('/tickets/:id', {
      templateUrl: '/ticket_view.html',
      controller: 'Edit'
    })
})
