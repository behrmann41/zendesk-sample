app.controller("Home", ["$scope","$http","$location",'myService', function($scope,$http,$location,myService){
  $scope.form = {};
  $scope.tickets = [];

  $http.get('/zendesk').then(function successCallback(response) {
    var tickets = response.data.tickets
    if (!tickets) {
      alert('no tickets found')
      return;
    }
    tickets.forEach(function(ticket){
      $scope.tickets.push({
        id: ticket.id,
        type: ticket.type || '',
        description: ticket.subject || ''
      })
    })
  }, function errorCallback(response){
    console.log("error response:", response);
  })

  $scope.editTicket = function (id) {
    var ticket = $scope.tickets.find(function(ticket) {
      return ticket.id === id
    })
    myService.set(ticket)
  };
  $scope.addTicket = function (){
    if (!$scope.form.type || !$scope.form.description) {
      alert('Please Include both a type and description')
      return;
    };
    $http({
      method: 'POST',
      url: '/zendesk',
      data: {
        "ticket": {
          "type": $scope.form.type,
          "comment": {
            "body": $scope.form.description
          },
          "subject": $scope.form.description
        }
      }
    }).then(function successCallback(response) {
      if (response.data.error) {
        return alert('error processing post');
      };
      var ticket = response.data.ticket
      $scope.tickets.push({id: ticket.id, type: ticket.type, description: ticket.description})
      }, function errorCallback(response) {
        alert('error processing post')
    })
    $scope.form = {};
  }
}])

app.controller('Edit', ['$scope', '$http', '$location','myService', function( $scope, $http, $location,myService) {
  $scope.ticket = myService.get();

  $scope.updateTicket = function() {
    $http({
      method: 'POST',
      url: `/zendesk/${$scope.ticket.id}`,
      data: {
        "ticket": {
          "type": $scope.ticket.type,
          "subject": $scope.ticket.description
        }
      }
    }).then(function successCallback(response) {
      if (response.data.error) {
        return alert('error processing post');
      };
      $location.path("/")
      }, function errorCallback(response) {
        alert('error processing post')
    })
  };
}])
