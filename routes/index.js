var express = require('express');
var router = express.Router();
var request = require('request');
var zendesk = require('node-zendesk');
var tokenString = new Buffer(`${process.env.ZEN_USERNAME}:${process.env.ZEN_PASSWORD}`).toString('base64');

router.post('/zendesk', function (req, res, next) {
  request.post({
    url:'https://iansteam.zendesk.com/api/v2/tickets.json',
    headers: {
      Authorization: 'Basic ' + tokenString,
      'Content-Type': 'application/json',
      Accept: 'application/json'
    },
    body: JSON.stringify(req.body)
  }, function (err, resp, body) {
    if (err) {
      return console.error('upload failed:', err);
    }
    res.send(body)
  });
})

router.get('/zendesk/', function(req, res, next) {
  request.get({
    url:'https://iansteam.zendesk.com/api/v2/tickets.json',
    headers: {
      Authorization: 'Basic ' + tokenString,
    },
  }, function (err, resp, body) {
    if (err) {
      return console.error('get failed:', err);
    }
    res.send(body)
  });
})

router.post('/zendesk/:id', function(req, res, next) {
  request.put({
    url:`https://iansteam.zendesk.com/api/v2/tickets/${req.params.id}.json`,
    headers: {
      Authorization: 'Basic ' + tokenString,
      'Content-Type': 'application/json',
      Accept: 'application/json'
    },
    body: JSON.stringify(req.body)
  }, function (err, resp, body) {
    if (err) {
      return console.error('get failed:', err);
    }
    res.send(body)
  });
})

/* GET home page. */
router.get('*', function(req, res, next) {
  res.sendFile('index.html', {
    root: __dirname + '/../public/'
  });
});

module.exports = router;
